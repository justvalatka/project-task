﻿using System;
using ProjectTask.Enums;
using ProjectTask.Models;
using ProjectTask.Interfaces;
using Xunit;
using NSubstitute;
using ProjectTask.Services;

namespace ServiceTests
{
    public class ServiceTests
    {
        // Provider is not VAT payer - VAT is not applicable
        [Theory]
        [InlineData(PersonTypes.Juridical, Locations.Argentina, false, 500, PersonTypes.Physical, Locations.Lithuania, true)]
        [InlineData(PersonTypes.Juridical, Locations.Latvia, false, 475, PersonTypes.Juridical, Locations.USA, false)]
        [InlineData(PersonTypes.Juridical, Locations.Spain, false, 200, PersonTypes.Physical, Locations.Germany, true)]
        [InlineData(PersonTypes.Juridical, Locations.USA, false, 850, PersonTypes.Juridical, Locations.France, false)]
        public void ProviderIsNonPVM_ShouldReturnWithoutPVM(PersonTypes providerType, Locations providerLocation, bool providerPVM,
            double price, PersonTypes clientType, Locations clientLocation, bool clientPVM)
        {
            // Arrange
            var service = Substitute.For<IInvoiceService>();
            var provider = new Person(providerType, providerLocation, providerPVM);
            var client = new Person(clientType, clientLocation, clientPVM);
            var expected = price;
            service.CalculateInvoice(provider, client, price).Returns(price);
            var testedService = new InvoiceService(service);

            // Act
            var actual = testedService.CalculateInvoice(provider, client, price);

            // Assert
            Assert.Equal(expected, actual);
        }

        // Provider is VAT payer, client is not from Europe - VAT is equal 0%
        [Theory]
        [InlineData(PersonTypes.Juridical, Locations.Argentina, true, 500, PersonTypes.Physical, Locations.Argentina, true)]
        [InlineData(PersonTypes.Juridical, Locations.Latvia, true, 475, PersonTypes.Juridical, Locations.USA, false)]
        [InlineData(PersonTypes.Juridical, Locations.Spain, true, 200, PersonTypes.Physical, Locations.Algeria, true)]
        [InlineData(PersonTypes.Juridical, Locations.USA, true, 850, PersonTypes.Juridical, Locations.Angola, false)]
        public void ClientIsNonEU_ShouldReturnWithoutPVM(PersonTypes providerType, Locations providerLocation, bool providerPVM,
            double price, PersonTypes clientType, Locations clientLocation, bool clientPVM)
        {
            // Arrange
            var service = Substitute.For<IInvoiceService>();
            var provider = new Person(providerType, providerLocation, providerPVM);
            var client = new Person(clientType, clientLocation, clientPVM);
            var expected = price;
            service.CalculateInvoice(provider, client, price).Returns(price);
            var testedService = new InvoiceService(service);

            // Act
            var actual = testedService.CalculateInvoice(provider, client, price);

            // Assert
            Assert.Equal(expected, actual);
        }

        // Provider is VAT payer, client: lives in Europe, is not paying VAT, but lives in different country than provider
        // Client country's VAT is applicable
        [Theory]
        [InlineData(PersonTypes.Juridical, Locations.Argentina, true, 500, PersonTypes.Physical, Locations.Lithuania, false)]
        [InlineData(PersonTypes.Juridical, Locations.Latvia, true, 475, PersonTypes.Juridical, Locations.Romania, false)]
        [InlineData(PersonTypes.Juridical, Locations.Spain, true, 200, PersonTypes.Physical, Locations.Portugal, false)]
        [InlineData(PersonTypes.Juridical, Locations.USA, true, 850, PersonTypes.Juridical, Locations.Estonia, false)]
        public void ClientIsEUNonPVMProviderFromDifferentLocation_ShouldReturnWithPVM(PersonTypes providerType, Locations providerLocation, bool providerPVM,
            double price, PersonTypes clientType, Locations clientLocation, bool clientPVM)
        {
            // Arrange
            var service = Substitute.For<IInvoiceService>();
            var provider = new Person(providerType, providerLocation, providerPVM);
            var client = new Person(clientType, clientLocation, clientPVM);
            var testedService = new InvoiceService(service);
            service.CalculateInvoice(provider, client, price)
                .Returns(price * (1 + (double)(int)Enum.Parse(typeof(PVM), client.location.ToString()) / 100));
            double expected = price * (1 + (double)(int)Enum.Parse(typeof(PVM), client.location.ToString()) / 100);

            // Act
            var actual = testedService.CalculateInvoice(provider, client, price);

            // Assert
            Assert.Equal(expected, actual);
        }

        // Provider is not VAT payer, client: lives in Europe, is paying VAT, but lives in different country than provider
        // VAT is not applicable
        [Theory]
        [InlineData(PersonTypes.Juridical, Locations.Spain, true, 500, PersonTypes.Physical, Locations.Lithuania, true)]
        [InlineData(PersonTypes.Juridical, Locations.Latvia, true, 475, PersonTypes.Juridical, Locations.Estonia, true)]
        [InlineData(PersonTypes.Juridical, Locations.Brazil, true, 200, PersonTypes.Physical, Locations.Germany, true)]
        [InlineData(PersonTypes.Juridical, Locations.USA, true, 850, PersonTypes.Juridical, Locations.Croatia, true)]
        public void ClientIsEUIsPVMProviderFromDifferentLocation_ShouldReturnWithoutPVM(PersonTypes providerType, Locations providerLocation, bool providerPVM,
            double price, PersonTypes clientType, Locations clientLocation, bool clientPVM)
        {
            // Arrange
            var service = Substitute.For<IInvoiceService>();
            var provider = new Person(providerType, providerLocation, providerPVM);
            var client = new Person(clientType, clientLocation, clientPVM);
            var expected = price;
            service.CalculateInvoice(provider, client, price).Returns(price);
            var testedService = new InvoiceService(service);

            // Act
            var actual = testedService.CalculateInvoice(provider, client, price);

            // Assert
            Assert.Equal(expected, actual);
        }

        // Provider is from the same country as client - VAT is always applicable
        [Theory]
        [InlineData(PersonTypes.Juridical, Locations.Tonga, true, 500, PersonTypes.Physical, Locations.Tonga, true)]
        [InlineData(PersonTypes.Juridical, Locations.Serbia, true, 475, PersonTypes.Juridical, Locations.Serbia, false)]
        [InlineData(PersonTypes.Juridical, Locations.Brazil, true, 200, PersonTypes.Physical, Locations.Brazil, true)]
        [InlineData(PersonTypes.Juridical, Locations.Poland, true, 850, PersonTypes.Juridical, Locations.Poland, false)]
        public void ClientAndProviderFromSameLocation_ShouldReturnPriceWithPVMAdded(PersonTypes providerType, Locations providerLocation, bool providerPVM,
            double price, PersonTypes clientType, Locations clientLocation, bool clientPVM)
        {
            // Arrange
            var service = Substitute.For<IInvoiceService>();
            var provider = new Person(providerType, providerLocation, providerPVM);
            var client = new Person(clientType, clientLocation, clientPVM);
            var testedService = new InvoiceService(service);
            service.CalculateInvoice(provider, client, price)
                .Returns(price * (1 + (double)(int)Enum.Parse(typeof(PVM), client.location.ToString()) / 100));
            double expected = price * (1 + (double)(int)Enum.Parse(typeof(PVM), client.location.ToString()) / 100);

            // Act
            var actual = testedService.CalculateInvoice(provider, client, price);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
