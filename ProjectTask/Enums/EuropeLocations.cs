﻿namespace ProjectTask.Enums
{
    public enum EuropeLocations
    {
        Spain = 1,
        Lithuania = 2,
        Estonia = 3,
        Latvia = 4,
        France = 5,
        Germany = 6,
        Italy = 7,
        Denmark = 8,
        Hungary = 9,
        Poland = 10,
        Portugal = 11,
        Croatia = 12,
        Belgium = 13,
        Bulgaria = 14,
        Austria = 15,
        Sweden = 16,
        Slovakia = 17,
        Slovenia = 18,
        Romania = 19,
        Serbia = 20,
    }
}
