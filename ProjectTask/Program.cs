﻿using System;
using ProjectTask.Enums;
using ProjectTask.Models;
using ProjectTask.Services;

namespace ProjectTask
{
    class Program
    {
        static void Main(string[] args)
        {
            InvoiceService service = new InvoiceService();
            Person provider = new Person(PersonTypes.Juridical, Locations.Germany, true);
            Person client = new Person(PersonTypes.Physical, Locations.Germany, true);
            double price = 300;

            price = service.CalculateInvoice(provider, client, price);
            service.ToString(provider, client, price);
            Console.ReadKey();
        }
    }
}
