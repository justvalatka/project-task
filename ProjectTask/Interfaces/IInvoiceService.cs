﻿using ProjectTask.Enums;
using ProjectTask.Models;

namespace ProjectTask.Interfaces
{
    public interface IInvoiceService
    {
        double CalculateInvoice(Person provider, Person client, double price);
        bool SameLocation(Person provider, Person client);
        bool LivesInEurope(Person person);
        double GetPVM(Locations location);
        void ToString(Person provider, Person client, double price);
    }
}
