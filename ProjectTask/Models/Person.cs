﻿using ProjectTask.Enums;

namespace ProjectTask.Models
{
    public class Person
    {
        public PersonTypes type { get; set; }
        public Locations location { get; set; }
        public bool paysPVM { get; set; }

        public Person(PersonTypes _type, Locations _location, bool _paysPVM)
        {
            type = _type;
            location = _location;
            paysPVM = _paysPVM;
        }
    }
}
