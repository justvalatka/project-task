﻿using System;
using ProjectTask.Enums;
using ProjectTask.Interfaces;
using ProjectTask.Models;

namespace ProjectTask.Services
{
    public class InvoiceService : IInvoiceService
    {
        private IInvoiceService _service;

        public InvoiceService(IInvoiceService mock)
        {
            _service = mock;
        }

        public InvoiceService()
        {
        }

        public double CalculateInvoice(Person provider, Person client, double price)
        {
            if (provider.paysPVM)
            {
                if (SameLocation(provider, client) || !client.paysPVM && !SameLocation(provider, client))
                {
                    //Console.WriteLine(price + price / 100 * GetPVM(client.location));
                    return (price + price / 100 * GetPVM(client.location));
                }
                if (!LivesInEurope(client) || client.paysPVM && !SameLocation(provider, client))
                {
                    //Console.WriteLine(price);
                    return price;
                }
            }

            //throw new InvalidOperationException("Provider does not pay VAT.");
            return price;
        }

        public bool SameLocation(Person provider, Person client)
        {
            return provider.location == client.location ? true : false;
        }

        public bool LivesInEurope(Person person)
        {
            return Enum.IsDefined(typeof(EuropeLocations), person.location.ToString()) ? true : false;
        }

        public double GetPVM(Locations location)
        {
            return (double)(int)Enum.Parse(typeof(PVM), location.ToString());
        }

        public void ToString(Person provider, Person client, double price)
        {
            var header = String.Format("{0,10}{1,17}{2,20}", "Type", "Location", "Is paying PVM");
            var output1 = String.Format("{0,12}{1,15}{2,15}", provider.type, provider.location, provider.paysPVM);
            var output2 = String.Format("{0,12}{1,15}{2,15}", client.type, client.location, client.paysPVM);
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine(header);
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine("                  PROVIDER                   ");
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine(output1);
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine("                   CLIENT                    ");
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine(output2);
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine("Total price is: {0}", price);
        }
    }
}
